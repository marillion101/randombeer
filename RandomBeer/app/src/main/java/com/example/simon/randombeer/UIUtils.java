package com.example.simon.randombeer;

import android.graphics.Rect;
import android.text.TextPaint;
import android.widget.TextView;

/**
 * Created by simon on 26/11/2017.
 * Utils class to find out if a TextView gets cropped.
 */

public class UIUtils {
    public static boolean testGetsCropped(TextView textView, String text) {
        TextPaint paint = textView.getPaint();
        Rect rect = new Rect();
        paint.getTextBounds(text, 0, text.length(), rect);
        return rect.height() > textView.getHeight() ||
                rect.width() > textView.getWidth();
    }
}
