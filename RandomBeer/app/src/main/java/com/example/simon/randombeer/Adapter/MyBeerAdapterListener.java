package com.example.simon.randombeer.Adapter;

/**
 * Created by simon on 21/11/2017.
 * Interface for the listener in the adapter
 */

public interface MyBeerAdapterListener {
    void rateBeer(int id);
    void goToBeer(int id);
}
