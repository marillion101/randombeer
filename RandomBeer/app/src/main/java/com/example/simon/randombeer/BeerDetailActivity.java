package com.example.simon.randombeer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.simon.randombeer.Dialogs.ErrorDialogBuilder;
import com.example.simon.randombeer.Dialogs.ErrorDialogListener;
import com.example.simon.randombeer.Dialogs.RatingDialog;
import com.example.simon.randombeer.Dialogs.RatingDialogListener;
import com.example.simon.randombeer.Task.DownloadImageListener;
import com.example.simon.randombeer.Task.DownloadImageTask;
import com.example.simon.randombeer.databinding.ActivityBeerDetailBinding;

import java.io.IOException;

public class BeerDetailActivity extends AppCompatActivity
        implements RatingDialogListener, DownloadImageListener, ErrorDialogListener {

    private final String TAG = BeerDetailActivity.class.getSimpleName();
    public static final String BEER_ID = "beer_id";

    private PunkAPIBeer beer;
    private ImageView beerImageView;
    private ActivityBeerDetailBinding binding;
    private ProgressBar progressBar;
    private LocalBeerDatabase db;

    //TODO: Database handling
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beer_detail);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_beer_detail);
        beerImageView = (ImageView) findViewById(R.id.detail_view_beer_image);
        progressBar = (ProgressBar) findViewById(R.id.detail_view_progress_bar);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        int id = intent.getIntExtra(BeerDetailActivity.BEER_ID, -1);
        db = new LocalBeerDatabase(this);

        if(id != -1) {

            beer = db.getBeer(id);
            if(beer != null) {
                binding.setBeer(beer);
                new DownloadImageTask(this).execute(beer.image_url);

                Button rankBtn = (Button) findViewById(R.id.button_did_drink);
                rankBtn.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        showRatingDialog();
                    }
                });
            } else {
                showBeerNotFoundError();
            }
        } else {
            showBeerNotFoundError();
        }

        setUpExplanationAlerts();
    }

    private void showBeerNotFoundError() {
        new ErrorDialogBuilder().setListener(this).
                show(getString(R.string.beer_not_found), this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_detail_menu, menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete_beer) {
            showDeleteConfirmation();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showRatingDialog() {
        RatingDialog ratingDialog = new RatingDialog(this, this);
        ratingDialog.setCancelable(true);
        ratingDialog.show();
    }

    // RatingDialogListener
    @Override
    public void onConfirm(float rating) {
        beer.didDrink = true;
        beer.rating = rating;
        db.updateBeer(beer);
        binding.invalidateAll();
        binding.executePendingBindings();
    }

    @Override
    public void onBitmapReceived(Bitmap bitmap) {
        if(bitmap != null) {
            beerImageView.setImageBitmap(bitmap);
            progressBar.setVisibility(View.GONE);
            beerImageView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onException(IOException e) {
        final String errorMessage = getString(R.string.connection_error);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ErrorDialogBuilder dialog = new ErrorDialogBuilder();
                dialog.show(errorMessage, BeerDetailActivity.this);
                beerImageView.setImageResource(R.mipmap.ic_beer_icon);
                progressBar.setVisibility(View.GONE);
                beerImageView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showDeleteConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.delelte_confirmation_alert));
        builder.setCancelable(true);
        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        db.delete(beer);
                        dialog.cancel();
                        finish();
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog deleteConfirmation = builder.create();
        deleteConfirmation.show();
    }

    @Override
    public void onDismissErrorDialog() {
        finish();
    }

    private void setUpExplanationAlerts() {
        TextView ibuTextView = (TextView) findViewById(R.id.detail_view_beer_ibu_name);
        TextView abvTextView = (TextView) findViewById(R.id.detail_view_beer_abv_name);
        TextView srmTextView = (TextView) findViewById(R.id.detail_view_srm_name);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Look at this dialog!")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();

        ibuTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.setMessage(getResources().getString(R.string.ibu_explanation));
                alert.show();
            }
        });

        abvTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.setMessage(getResources().getString(R.string.abv_explanation));
                alert.show();
            }
        });

        srmTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.setMessage(getResources().getString(R.string.srm_explanation));
                alert.show();
            }
        });
    }
}
