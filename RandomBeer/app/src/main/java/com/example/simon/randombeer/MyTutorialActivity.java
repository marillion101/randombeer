package com.example.simon.randombeer;

import android.content.Intent;
import android.os.Bundle;

import com.hololo.tutorial.library.Step;
import com.hololo.tutorial.library.TutorialActivity;

/**
 * Created by simon on 05/12/2017.
 */

public class MyTutorialActivity extends TutorialActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        addFragment(new Step.Builder().setTitle("Welcome to RandomBeer")
                .setContent("With this app you can discover and experience new and different beers.")
                .setBackgroundColor(getResources().getColor(R.color.primaryTutorialColor)) // int background color
                .setSummary("Use the 'Get a beer' button to discover beers.")
                .setDrawable(R.drawable.fetchbeer) // int top drawable
                .build());
        addFragment(new Step.Builder().setTitle("Save your beers")
                .setContent("If you like a beer you can save it, and come back to it later.")
                .setBackgroundColor(getResources().getColor(R.color.secondaryColor)) // int background color
                .setSummary("To save just push the 'Save' button. It will turn green on success.")
                .setDrawable(R.drawable.fetchbeersave) // int top drawable
                .build());
        addFragment(new Step.Builder().setTitle("Share with friends")
                .setContent("With RandomBeer you can share a beer with your friends and make plans to try it later.")
                .setBackgroundColor(getResources().getColor(R.color.primaryTutorialColor)) // int background color
                .setSummary("Use the 'Share' button to share the beer via an other app.")
                .setDrawable(R.drawable.fetchbeershare) // int top drawable
                .build());
        addFragment(new Step.Builder().setTitle("Browse your beers")
                .setContent("You can find all the beers you saved under the 'MyBeers' tab.")
                .setBackgroundColor(getResources().getColor(R.color.secondaryColor)) // int background color
                .setDrawable(R.drawable.mybeers) // int top drawable
                .build());
        addFragment(new Step.Builder().setTitle("Rate your beers")
                .setContent("After you tried a beer you can rate it and find out which you liked best.")
                .setBackgroundColor(getResources().getColor(R.color.primaryTutorialColor)) // int background color
                .setSummary("You can rate a beer after pushing the 'I did drink this button'.")
                .setDrawable(R.drawable.ratebeer) // int top drawable
                .build());
    }

    @Override
    public void finishTutorial() {
        // Your implementation
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
