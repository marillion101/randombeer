package com.example.simon.randombeer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.CursorAdapter;

/**
 * Created by simon on 31/10/2017.
 * Database class
 */

public class LocalBeerDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "SQLiteBeers.db";
    private static final int DATABASE_VERSION = 3;
    public static final String BEER_TABLE_NAME = "APIBeers";
    public static final String ADAPTER_ID_COLUMN = "_id";
    public static final String ID_BEER_COLUMN = "id";
    public static final String NAME_COLUMN = "name";
    public static final String TAGLINE_COLUMN = "tagline";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String FIRST_BREWED_COLUMN = "first_brewed";
    public static final String IMAGE_URL_BEER_COLUMN = "image_url";
    public static final String ABV_BEER_COLUMN = "abv";
    public static final String IBU_BEER_COLUMN = "ibu";
    public static final String SRM_BEER_COLUMN = "srm";
    public static final String RATING_BEER_COLUMN = "rating";
    public static final String DID_DRINK_BEER_COLUMN = "diddrink";


    public LocalBeerDatabase(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + BEER_TABLE_NAME + "(" + ID_BEER_COLUMN + " INTEGER, " +
                ADAPTER_ID_COLUMN + " INTEGER PRIMARY KEY, " +
                NAME_COLUMN + " TEXT, " +
                TAGLINE_COLUMN + " TEXT," +
                DESCRIPTION_COLUMN + " TEXT," +
                FIRST_BREWED_COLUMN + " TEXT," +
                IMAGE_URL_BEER_COLUMN + " TEXT, " +
                ABV_BEER_COLUMN + " REAL," +
                IBU_BEER_COLUMN + " REAL," +
                SRM_BEER_COLUMN + " REAL," +
                RATING_BEER_COLUMN + " REAL," +
                DID_DRINK_BEER_COLUMN + " INTEGER)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_NAME);
        onCreate(sqLiteDatabase);
    }

    public boolean insert(PunkAPIBeer beer) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = beerToContentValue(beer);
        db.insert(BEER_TABLE_NAME, null, contentValues);
        return true;
    }



    private PunkAPIBeer CursorToBeer(Cursor cur) {
        if(cur.getCount() > 0 ) {
            cur.moveToFirst();
            PunkAPIBeer beer = new PunkAPIBeer();
            beer.id = cur.getInt(cur.getColumnIndexOrThrow(ID_BEER_COLUMN));
            beer.name = cur.getString(cur.getColumnIndexOrThrow(NAME_COLUMN));
            beer.tagline = cur.getString(cur.getColumnIndexOrThrow(TAGLINE_COLUMN));
            beer.first_brewed = cur.getString(cur.getColumnIndexOrThrow(FIRST_BREWED_COLUMN));
            beer.description = cur.getString(cur.getColumnIndexOrThrow(DESCRIPTION_COLUMN));
            beer.image_url = cur.getString(cur.getColumnIndexOrThrow(IMAGE_URL_BEER_COLUMN));
            beer.abv = cur.getDouble(cur.getColumnIndexOrThrow(ABV_BEER_COLUMN));
            beer.ibu = cur.getDouble(cur.getColumnIndexOrThrow(IBU_BEER_COLUMN));
            beer.srm = cur.getDouble(cur.getColumnIndexOrThrow(SRM_BEER_COLUMN));
            beer.rating = cur.getFloat(cur.getColumnIndexOrThrow(RATING_BEER_COLUMN));
            int didDrink = cur.getInt(cur.getColumnIndexOrThrow(DID_DRINK_BEER_COLUMN));
            beer.didDrink = (didDrink == 1);
            return beer;
        }
        return null;

    }

    public boolean updateBeer(PunkAPIBeer beer) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = beerToContentValue(beer);
        db.update(BEER_TABLE_NAME, contentValues, ID_BEER_COLUMN + " = ? ", new String[] { Integer.toString(beer.id) } );
        return true;
    }

    public PunkAPIBeer getBeer(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM " + BEER_TABLE_NAME + " WHERE " +
                ID_BEER_COLUMN + " = ? ", new String[] { Integer.toString(id) } );
        return CursorToBeer(res);
    }
    public Cursor getAllBeers() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery( "SELECT * FROM " + BEER_TABLE_NAME, null );
        return res;
    }

    /**
     * Get a cursor with all beers, that contain the subString in their name.
     * This is used when searching.
     * @param subString
     * @return
     */
    public Cursor getBeerWithName(String subString) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectionArgs[];
        selectionArgs= new String[] {"%"+ subString + "%" };
        Cursor res = db.rawQuery( "SELECT * FROM " + BEER_TABLE_NAME +
                " WHERE " + NAME_COLUMN + " LIKE ?", selectionArgs );
        return res;
    }

    public Integer delete(PunkAPIBeer beer) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(BEER_TABLE_NAME,
                ID_BEER_COLUMN + " = ? ",
                new String[] { Integer.toString(beer.id) });
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(BEER_TABLE_NAME, null, null);
    }

    private ContentValues beerToContentValue(PunkAPIBeer beer) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID_BEER_COLUMN, beer.id);
        contentValues.put(NAME_COLUMN, beer.name);
        contentValues.put(TAGLINE_COLUMN, beer.tagline);
        contentValues.put(FIRST_BREWED_COLUMN, beer.first_brewed);
        contentValues.put(DESCRIPTION_COLUMN, beer.description);
        contentValues.put(IMAGE_URL_BEER_COLUMN, beer.image_url);
        contentValues.put(ABV_BEER_COLUMN, beer.abv);
        contentValues.put(IBU_BEER_COLUMN, beer.ibu);
        contentValues.put(SRM_BEER_COLUMN, beer.srm);
        contentValues.put(RATING_BEER_COLUMN, beer.rating);
        contentValues.put(DID_DRINK_BEER_COLUMN, beer.didDrink);
        return contentValues;
    }


}
