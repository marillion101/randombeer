package com.example.simon.randombeer.Task;

import android.graphics.Bitmap;

import java.io.IOException;

/**
 * Created by simon on 05/11/2017.
 * Listener for the download image task
 */

public interface DownloadImageListener {
    void onBitmapReceived(Bitmap bitmap);
    void onException(IOException e);
}
