package com.example.simon.randombeer.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.simon.randombeer.LocalBeerDatabase;
import com.example.simon.randombeer.R;

/**
 * Created by simon on 21/11/2017.
 * Custom version of the CursorAdapter to display the beers.
 * This class takes care of the layout and the clicks on the view item.
 */

public class MyBeerCursorAdapter extends CursorAdapter {

    private static String TAG = MyBeerCursorAdapter.class.getSimpleName();
    private final MyBeerAdapterListener listener;

    public MyBeerCursorAdapter(Context context, Cursor cursor, MyBeerAdapterListener listener) {
        super(context, cursor, 0);
        this.listener = listener;
    }

    // The newView method is used to inflate a new view and return it,
    // you don't bind any data to the view at this point.
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_view_item, parent, false);
    }

    // The bindView method is used to bind all data to a given view
    // such as setting the text on a TextView.
    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        // get the info needed to display
        final int id = cursor.getInt(cursor.getColumnIndexOrThrow(LocalBeerDatabase.ID_BEER_COLUMN));
        String tagline = cursor.getString(cursor.getColumnIndexOrThrow(LocalBeerDatabase.TAGLINE_COLUMN));
        String name = cursor.getString(cursor.getColumnIndexOrThrow(LocalBeerDatabase.NAME_COLUMN));
        boolean didDrink = 0 != cursor.getInt(cursor.getColumnIndexOrThrow(LocalBeerDatabase.DID_DRINK_BEER_COLUMN));

        // find the ui elements
        TextView tagLineLabel = (TextView)view.findViewById(R.id.tagline_label);
        TextView nameLabel = (TextView) view.findViewById(R.id.beerName_label);
        ImageView checkBox = (ImageView) view.findViewById(R.id.beerDidDrink_checkbox);

        // update the content of the ui elements with the new information.
        tagLineLabel.setText(tagline);
        nameLabel.setText(name);
        if(didDrink) {
            checkBox.setImageResource(R.drawable.ic_beer_glass_empty);

        } else {
            checkBox.setImageResource(R.drawable.ic_beer_glass_full);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.rateBeer(id);
                }
            });
        }

        // set click listener to go to detail view.
        View.OnClickListener detailViewListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.goToBeer(id);
            }
        };

        nameLabel.setOnClickListener(detailViewListener);
        tagLineLabel.setOnClickListener(detailViewListener);
    }
}
