package com.example.simon.randombeer.Fragments;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.simon.randombeer.Dialogs.ErrorDialogBuilder;
import com.example.simon.randombeer.LocalBeerDatabase;
import com.example.simon.randombeer.MyTutorialActivity;
import com.example.simon.randombeer.PunkAPIBeer;
import com.example.simon.randombeer.R;
import com.example.simon.randombeer.Task.DownloadImageListener;
import com.example.simon.randombeer.Task.DownloadImageTask;
import com.example.simon.randombeer.Task.PunkAPIRequest;
import com.example.simon.randombeer.Task.PunkAPIRequestListener;
import com.example.simon.randombeer.UIUtils;
import com.example.simon.randombeer.databinding.FetchBeerFragmentBinding;

import org.json.JSONException;

import java.io.IOException;


/**
 * Fragment to fetch the beers from the api
 */
public class FetchBeerFragment extends Fragment implements PunkAPIRequestListener, DownloadImageListener {

    private static final String TAG = FetchBeerFragment.class.getSimpleName();

    private Button randomBeerButton;
    private Button shareButton;
    private Button saveButton;

    private ImageView imageView;
    private ProgressBar progressBar;
    private ProgressDialog progressDialog;

    private TextView descriptionTextView;

    FetchBeerFragmentBinding dataBinding;
    private PunkAPIBeer curBeer;
    private boolean curBeerInDB = false;
    private LocalBeerDatabase dbHelper;
    private DownloadImageTask imageTask = null;

    public FetchBeerFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // setup the databinding
        dataBinding = FetchBeerFragmentBinding.inflate(getActivity().getLayoutInflater());
        View view = dataBinding.getRoot();

        // set up the buttons
        randomBeerButton = (Button) view.findViewById(R.id.random_beer_button);
        randomBeerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchAPIRequest();
            }
        });

        shareButton = (Button) view.findViewById(R.id.share_button);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareBeer();
            }
        });

        saveButton = (Button) view.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveBeerToDB();
            }
        });

        curBeerInDB = false;
        // get reference to progress bar and image view.
        progressBar = (ProgressBar) view.findViewById(R.id.curr_beer_image_progress_bar);
        imageView = (ImageView) view.findViewById(R.id.curr_beer_image);

        // set up the description text view with pop up.
        descriptionTextView = (TextView) view.findViewById(R.id.detail_view_beer_description);
        descriptionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(UIUtils.testGetsCropped(descriptionTextView,
                        String.valueOf(descriptionTextView.getText()))) {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                            .setCancelable(true)
                            .setMessage(curBeer.description)
                            .create();
                    alertDialog.show();
                }

            }
        });

        // set up the progress dialog
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(R.string.loading_title);
        progressDialog.setMessage(getResources().getString(R.string.plase_wait_message));
        progressDialog.setCancelable(false);

        setUpExplanationAlerts(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        // find out if the tutorial should be displayed. This should only be displayed when
        // first starting the app or if this preference is reset in the user settings screen.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        boolean shouldShowTutorial = prefs.getBoolean(getString(R.string.show_explanations), true);
        if(shouldShowTutorial) {
            SharedPreferences.Editor edit = prefs.edit();
            edit.putBoolean(getString(R.string.show_explanations), false);
            edit.commit();
            // start tutorial
            Intent intent = new Intent(getActivity(), MyTutorialActivity.class);
            startActivity(intent);

        } 
    }

    // Buttons methods
    public void saveBeerToDB() {
        if(curBeer != null) {
            dbHelper = new LocalBeerDatabase(getActivity());
            dbHelper.insert(curBeer);
            dbHelper.close();
            curBeerInDB = true;
            dataBinding.invalidateAll();
            saveButton.setEnabled(false);
            saveButton.setText(R.string.button_title_save_successful);
            saveButton.setBackgroundColor(getResources().getColor(R.color.colorSuccess));

        }
    }


    public void shareBeer() {
        if(curBeer != null) {
            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String text = getResources().getString(R.string.share_beer_message) +
                    " \n" + curBeer.name + "\n" + curBeer.tagline;
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);
            startActivity(Intent.
                    createChooser(sharingIntent, getResources().getString(R.string.share_title)));
        }
    }

    // PunkAPIRequestListener methods
    @Override
    public void onResponseParsed(PunkAPIBeer[] beers) {
        PunkAPIBeer first = beers[0];

        if(first != null) {
            dataBinding.setCurBeer(first);
            curBeer = first;
            curBeerInDB = false;
            progressDialog.dismiss();
            dataBinding.invalidateAll();

            setButtonEnabled(true);
            saveButton.setBackgroundColor(getResources().getColor(R.color.primaryColor));
            saveButton.setText(R.string.button_title_save);
            fetchCurrBeerImg();

        }
    }

    @Override
    public void onException(final Exception exception) {
        handleException(exception);
    }


    // DownloadImageListener
    @Override
    public void onBitmapReceived(Bitmap bitmap) {
        if(bitmap != null) {
            imageView.setImageBitmap(bitmap);
            progressBar.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setImageResource(R.mipmap.ic_beer_icon);
        }
        imageTask = null;
    }

    @Override
    public void onException(IOException exception) {
        handleException(exception);
    }

    // fetch request functions
    private void fetchAPIRequest() {
        PunkAPIRequest request = new PunkAPIRequest(this);
        request.execute();
        progressDialog.show();
        setButtonEnabled(false);
    }

    private void fetchCurrBeerImg() {
        if(imageTask != null) {
            imageTask.cancel(true);
        }
        if(curBeer != null) {
            imageTask = new DownloadImageTask(this);
            imageTask.execute(curBeer.image_url);
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    // try to show the correct error message to the user.
    private void handleException(final Exception exception) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // clean up after failed request;
                progressDialog.dismiss();
                setButtonEnabled(true);
                progressBar.setVisibility(View.GONE);

                // Show error message to user.
                ErrorDialogBuilder builder = new ErrorDialogBuilder();
                if(JSONException.class.isAssignableFrom(exception.getClass())) {
                    builder.show(getString(R.string.invalid_server_response), getActivity());
                } else if(IOException.class.isAssignableFrom(exception.getClass())) {
                    builder.show(getString(R.string.connection_error), getActivity());
                } else {
                    builder.show(getString(R.string.unknown_error_message), getActivity());
                }
            }
        });
    }

    private void setUpExplanationAlerts(View view) {
        TextView ibuTextView = (TextView) view.findViewById(R.id.detail_view_beer_ibu_name);
        TextView abvTextView = (TextView) view.findViewById(R.id.detail_view_beer_abv_name);
        TextView srmTextView = (TextView) view.findViewById(R.id.detail_view_srm_name);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Look at this dialog!")
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();

        ibuTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.setMessage(getResources().getString(R.string.ibu_explanation));
                alert.show();
            }
        });

        abvTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.setMessage(getResources().getString(R.string.abv_explanation));
                alert.show();
            }
        });

        srmTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.setMessage(getResources().getString(R.string.srm_explanation));
                alert.show();
            }
        });
    }

    private void setButtonEnabled(boolean enabled) {
        shareButton.setEnabled(enabled);
        saveButton.setEnabled(enabled);
        randomBeerButton.setEnabled(enabled);
    }
}
