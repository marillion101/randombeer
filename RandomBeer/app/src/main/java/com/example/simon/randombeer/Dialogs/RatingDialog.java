package com.example.simon.randombeer.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.simon.randombeer.R;

/**
 * Created by simon on 03/11/2017.
 * Custom rating dialog to let the user rate a beer
 * with 1 - 5 stars.
 */

public class RatingDialog extends Dialog {

    private RatingBar ratingBar;
    private RatingDialogListener listener;
    private TextView titleTextView;

    public RatingDialog(@NonNull Context context) {
        super(context);
        initialize();
    }

    public RatingDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        initialize();
    }

    public RatingDialog(@NonNull Context context, RatingDialogListener listener) {
        super(context);
        initialize();
        this.listener = listener;
    }

    public RatingDialog(@NonNull Context context, int themeResId, RatingDialogListener listener) {
        super(context, themeResId);
        initialize();
        this.listener = listener;
    }

    protected RatingDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        initialize();
    }

    public void setRating(float rating) {
        ratingBar.setRating(rating);
    }

    public void setListener(RatingDialogListener listener) {
        this.listener = listener;
    }

    private void initialize() {
        this.setContentView(R.layout.star_rating_dialog);
        ratingBar = (RatingBar)this.findViewById(R.id.rating_dialog_ratingbar);
        ratingBar.setRating(2.5f);
        titleTextView = (TextView) findViewById(R.id.rating_dialog_title);

        titleTextView.setText(R.string.title_rating_dialog);

        Button cancelButton = (Button) findViewById(R.id.rating_dialog_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Button confirmButton = (Button) findViewById(R.id.rating_dialog_confirm_button);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null) {
                    listener.onConfirm(ratingBar.getRating());
                }
                dismiss();
            }
        });

    }
}
