package com.example.simon.randombeer.Dialogs;

/**
 * Created by simon on 03/11/2017.
 * Listener for the rating dialog.
 */

public interface RatingDialogListener {
    // this listener function gets called when the user set a rating in
    // the dialog and confirmed it.
    void onConfirm(float rating);
}
