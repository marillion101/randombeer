package com.example.simon.randombeer;

/**
 * Created by simon on 29/10/2017.
 * Model class used to store data form the API.
 */

public class PunkAPIBeer {
    public int id;
    public String name;
    public String tagline;
    public String first_brewed;
    public String description;
    public String image_url;
    public double abv;
    public double ibu;
    public double srm;

    public float rating;
    public boolean didDrink;

    public String getFormattedABV() {
        return String.format("%.1f", abv);
    }

    public String getFormattedIBU() {
        return String.format("%.1f", ibu);
    }

    public String getFormattedSRM() {
        return String.format("%.1f", srm);
    }
}
