package com.example.simon.randombeer.Dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.simon.randombeer.R;

/**
 * Created by simon on 05/11/2017.
 * Class for building error messages
 * and informing the user about what went wrong.
 */

public class ErrorDialogBuilder {

    private ErrorDialogListener listener;

    public  void show(Exception e, Context context) {
        show(findMessageForError(e), context);
    }

    public ErrorDialogBuilder setListener(ErrorDialogListener listener) {
        this.listener = listener;
        return this;
    }

    public void show(String errorMessage, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(errorMessage)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if(listener != null) {
                            listener.onDismissErrorDialog();
                        }
                    }
                });
        AlertDialog alert = builder.create();
        try {
            alert.show();
        }catch (Exception e) {
            // some errors cause the view/ activity to disappear
            // this try/ catch prevents the app from crashing if it
            // wants to show an error on a view that has already disappeared.
        }

    }

    private String findMessageForError(Exception e) {
        return "Generic Error Message";
    }
}
