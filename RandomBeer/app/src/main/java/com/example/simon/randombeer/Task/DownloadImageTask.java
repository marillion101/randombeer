package com.example.simon.randombeer.Task;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by simon on 05/11/2017.
 * This class is responsible to download an image from the Punk API
 * It is a subclass of the AsyncTask.
 */

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    public int msUntilTimeOut = 5000;

    private DownloadImageListener delegate;

    public DownloadImageTask(DownloadImageListener delegate) {
        this.delegate = delegate;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected Bitmap doInBackground(String... urls) {
        String imageUrl = urls[0];
        Bitmap bitmapImg = null;
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setConnectTimeout(msUntilTimeOut);
            connection.setReadTimeout(msUntilTimeOut);
            connection.connect();
            InputStream input = connection.getInputStream();
            bitmapImg = BitmapFactory.decodeStream(input);
        } catch ( IOException e) {
            if(delegate != null) {
                delegate.onException(e);
            }
        }
        return bitmapImg;
    }

    protected void onPostExecute(Bitmap result) {
        if(delegate != null) {
            delegate.onBitmapReceived(result);
        }
    }
}
