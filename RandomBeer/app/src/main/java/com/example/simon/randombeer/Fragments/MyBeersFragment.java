package com.example.simon.randombeer.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;

import com.example.simon.randombeer.Adapter.MyBeerAdapterListener;
import com.example.simon.randombeer.Adapter.MyBeerCursorAdapter;
import com.example.simon.randombeer.BeerDetailActivity;
import com.example.simon.randombeer.Dialogs.RatingDialog;
import com.example.simon.randombeer.Dialogs.RatingDialogListener;
import com.example.simon.randombeer.LocalBeerDatabase;
import com.example.simon.randombeer.PunkAPIBeer;
import com.example.simon.randombeer.R;


/**
 * Fragment to display a listview of the saved beers.
 */
public class MyBeersFragment extends Fragment
        implements MyBeerAdapterListener, RatingDialogListener, TextWatcher {

    public static final String BEER_NAME = "beername";

    private String TAG = MyBeersFragment.class.getSimpleName();
    private ListView listView;
    private LocalBeerDatabase dbHelper;
    private MyBeerCursorAdapter cursorAdapter;
    private int tempId = -1;
    private EditText searchField;

    public MyBeersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.my_beers_fragment, container, false);
        setHasOptionsMenu(true);

        dbHelper = new LocalBeerDatabase(getActivity());

        // Setting up the Listview with a Cursoradapter
        final Cursor cursor = dbHelper.getAllBeers();
        cursorAdapter = new MyBeerCursorAdapter(getActivity(), cursor, this);
        listView = (ListView)view.findViewById(R.id.listView);
        listView.setAdapter(cursorAdapter);
        listView.setEmptyView(view.findViewById(R.id.listview_empty_message));

        // Adding Search/ Filter functionality to the Adapter
        cursorAdapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {
                String nameQuery = charSequence.toString();
                return dbHelper.getBeerWithName(nameQuery);
            }
        });
        setUpSearchField(view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my_beers_menu, menu);
        for(int i = 0; i < menu.size(); i++){
            colorDrawable(menu.getItem(i).getIcon());
        }
        super.onCreateOptionsMenu(menu, inflater);
    }


    // display either search icon or cross icon depending on the search.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.toggle_search_field) {
            if(searchField.getVisibility() == View.VISIBLE) {
                // go out of search mode
                searchField.setText("");
                searchField.setVisibility(View.GONE);
                item.setIcon(android.R.drawable.ic_menu_search);
                // Dismiss the keyboard if the search is dismissed
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(searchField.getWindowToken(), 0);
            } else {
                // go into search mode
                searchField.setText("");
                searchField.setVisibility(View.VISIBLE);
                item.setIcon(android.R.drawable.ic_menu_close_clear_cancel);

                // focus search field
                searchField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN , 0, 0, 0));
                searchField.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP , 0, 0, 0));
                // show keyboard
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(searchField, InputMethodManager.SHOW_IMPLICIT);
            }
            // Consume this click here
            colorDrawable(item.getIcon());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onResume() {
        super.onResume();
        dbHelper = new LocalBeerDatabase(getActivity());
        final Cursor cursor = dbHelper.getAllBeers();
        cursorAdapter.changeCursor(cursor);
        if(!searchField.getText().equals("")) {
            cursorAdapter.getFilter().filter(searchField.getText());
            cursorAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        dbHelper.close();
    }

    @Override
    public void onPause() {
        super.onPause();
        dbHelper.close();
    }

    // MyBeerAdapterListener methods
    @Override
    public void rateBeer(int id) {
        tempId = id;
        RatingDialog ratingDialog = new RatingDialog(getActivity(), this);
        ratingDialog.setCancelable(true);
        ratingDialog.show();
    }

    @Override
    public void goToBeer(int id) {
        Intent intent = new Intent(getActivity(), BeerDetailActivity.class);
        intent.putExtra(BeerDetailActivity.BEER_ID, id);
        startActivity(intent);
    }

    // RatingDialogListener
    @Override
    public void onConfirm(float rating) {
        PunkAPIBeer beer = dbHelper.getBeer(tempId);
        beer.didDrink = true;
        beer.rating = rating;
        dbHelper.updateBeer(beer);
        dbHelper = new LocalBeerDatabase(getActivity());
        final Cursor cursor = dbHelper.getAllBeers();
        cursorAdapter.changeCursor(cursor);
    }

    // TextWatcher functions
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        cursorAdapter.getFilter().filter(charSequence);
        cursorAdapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    // private functions
    private void setUpSearchField(View view) {
        searchField = (EditText) view.findViewById(R.id.search_field);
        searchField.addTextChangedListener(this);
    }

    private void colorDrawable(Drawable drawable ) {
        if(drawable != null) {
            drawable.mutate();
            drawable.setColorFilter(getResources().getColor(R.color.primaryTextColor), PorterDuff.Mode.SRC_ATOP);
        }
    }
}
