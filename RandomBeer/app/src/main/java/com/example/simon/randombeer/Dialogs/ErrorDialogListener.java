package com.example.simon.randombeer.Dialogs;

/**
 * Created by simon on 05/11/2017.
 * Interface to let a view know that the user dismissed the error message
 */

public interface ErrorDialogListener {
    void onDismissErrorDialog();
}
