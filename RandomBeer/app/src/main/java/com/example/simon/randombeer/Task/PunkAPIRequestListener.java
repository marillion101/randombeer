package com.example.simon.randombeer.Task;

import com.example.simon.randombeer.PunkAPIBeer;

/**
 * Created by simon on 29/10/2017.
 * Listener for the PunkAPIRequest
 */

public interface PunkAPIRequestListener {
    void onResponseParsed(PunkAPIBeer[] beers);
    void onException(Exception exception);

}
