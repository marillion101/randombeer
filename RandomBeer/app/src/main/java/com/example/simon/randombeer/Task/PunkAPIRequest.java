package com.example.simon.randombeer.Task;

import android.os.AsyncTask;

import com.example.simon.randombeer.PunkAPIBeer;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by simon on 29/10/2017.
 * This class is responsible for the request to the punk API
 * This is a subclass of the AsyncTask.
 */

public class PunkAPIRequest extends AsyncTask<Void, Void, JSONArray> {

    private static final String urlString = "https://api.punkapi.com/v2/beers/random";
    private PunkAPIRequestListener listener;

    public PunkAPIRequest(PunkAPIRequestListener listener) {
        this.listener = listener;
    }


    @Override
    protected JSONArray doInBackground(Void... voids) {
        URL url;
        HttpURLConnection connection = null;
        JSONArray jsonResponse = null;

        try {
            url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            StringBuilder stringBuilderResponse = parseHTTPResponse(connection);
            jsonResponse = new JSONArray(stringBuilderResponse.toString());

        }  catch (IOException | JSONException exception) {
            if(listener != null) {
                listener.onException(exception);
            }
        }  finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
        return jsonResponse;
    }

    @Override
    protected void onPostExecute(final JSONArray response) {
        final Gson gson = new Gson();
        if(response != null) {
            PunkAPIBeer[] beers = gson.fromJson(response.toString(), PunkAPIBeer[].class);
            listener.onResponseParsed(beers);
        }
    }

    private StringBuilder parseHTTPResponse(HttpURLConnection connection) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line + "\n");
        }
        bufferedReader.close();
        return stringBuilder;
    }
}
